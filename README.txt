INTRODUCTION
------------
This module provides simple block with next information:
 - short info about user (username, user picture) linked to user profile.
 - "New friends" link to the list of pending friend requests (/user/%/friends/pending), provided by the Flag friend module (and Views of course).
 - "New messages" link to the messages section (/messages), provided by the Private messages module.
 - "Log out" link.

REQUIREMENTS
------------
This module requires the following modules:
 * Flag Friend (https://drupal.org/project/flag_friend)
 * Privatemsg (https://www.drupal.org/project/privatemsg)

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.
 * Enable block "New messages and New friend requests" on block
   administration page (/admin/structure/block).

CONFIGURATION
-------------
 * By default each logged in user will see the block.
 * You can copy profile-news-block.tpl.php file from module folder
   to your theme folder and change it however you want or just use css to customize it.

MAINTAINER
-----------
 * Taras Kyryliuk (JGoover) - https://drupal.org/user/2901135