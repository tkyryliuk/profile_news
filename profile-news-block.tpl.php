<?php
/**
 * @file
 * HTML for a block that shows  new messages and new friend requests (pending).
 *
 * Available variables:
 * - $classes: String of classes that can be used to style contextually through CSS.
 *   It can be manipulated through the variable $classes_array from preprocess functions.
 *   Notices: classes 'friends-notice' and 'messages-notice' will be added to $classes_array
 *   when there are any new messages or friends requests (so their can be used in css).
 * - $user_name: renderable element that shows username with 'username' theme hook.
 * - $user_pic: renderable element that shows user avatar with 'user_picture' theme hook.
 * - $new_friends: string with number of new pending friend requests.
 * - $new_messages: string with number of new unread messages.
 * - $classes_array: Array of html class attribute values.
 * It is flattened into a string within the variable $classes.
 *
 * Other variables:
 * The following variables are provided for contextual information.
 * - $user: The user object of the node author.
 */
?>

<div class="<?php print $classes; ?>">
  <div class="user-pic"><?php print render($user_pic); ?></div>
  <div class="user-name"><?php print render($user_name); ?></div>
  <div class="new-friends"><?php print t('Friend requests:') . ' <a href="/user/' . $user->uid . '/friends/pending">(' . $new_friends . ')</a>'; ?></div>
  <div class="new-msg"><?php print t('New messages:') . ' <a href="/messages">(' . $new_messages . ')</a>'; ?></div>
  <div class="logout"><?php print l(t('Log out'), 'user/logout'); ?></div>
</div>